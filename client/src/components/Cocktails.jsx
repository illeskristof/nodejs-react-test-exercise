import './Cocktails.css';
import { useState, useEffect } from "react";

function Cocktails() {
  const [cocktail, setCocktail] = useState({})
  const [loaded, setLoaded] = useState(false)
  const [searchTerm, setSearchTerm] = useState('')
  const [error, setError] = useState(false)
  const ingredients = loaded ? cocktail.ingredients.map(ingredient => {
    return <li key={ingredient.ingredient}>{ingredient.ingredient}: {ingredient.measure}</li>
  }) : ''

  const fetchApi = () => {
    let url = searchTerm ? `http://localhost:5000/api/cocktail?name=${searchTerm}` : "http://localhost:5000/api/cocktail"
    setLoaded(false)
    setError(false)
    fetch(url)
      .then(res => res.json())
      .then(res => {
        if (res.status === 500) {
          setError(true)
        } else {
          setCocktail(res)
        }
        setLoaded(true)
        setSearchTerm('')
      })
      .catch(e => setError(true))
  }

  useEffect(() => {
    fetchApi()
  }, [])

  return (
    <div className="card">
      {
        loaded ? 
            error ? <div className="error">
                      Error
                      <button className="tryAgain" onClick={fetchApi}>Try again</button>
                    </div>
            :
            <div className="cardBody">
              <div className="header">
                <div className="name">
                  { cocktail.name }
                </div>
                <img src={cocktail.thumb} className="thumb" alt={cocktail.name}/>
              </div>
              <div className="ingredients">Ingredients:</div>
              <ul>
                {
                  ingredients
                }
              </ul>
              <div className="instructions">Instructions:</div>
              <div className="instruction">
                {
                  cocktail.instructions
                }
              </div>
              <div className="actions">
                <button onClick={fetchApi}>Get new random recipe</button>
                <div className="search">
                <input placeholder="search cocktail by name" value={searchTerm} onChange={(e => setSearchTerm(e.target.value))}></input>
                  {
                    searchTerm ? <button onClick={fetchApi}>Search</button> : ''
                  }
                </div>
              </div>
            </div>
        : <div />
      }
    </div>
  )
}

export default Cocktails;