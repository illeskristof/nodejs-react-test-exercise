import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Cocktails from './components/Cocktails'

function App() {
  return (
    <Router>
      <div>
        <Switch>
          <Route path="/cocktail">
            <Cocktails />
          </Route>
          <Route path="/">
            <Redirect to="/cocktail" />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
