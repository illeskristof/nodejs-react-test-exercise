const express = require('express')
const fetch = require('node-fetch')
const cors = require('cors')
const app = express()
const port = 5000

app.use(cors())

app.get('/api/cocktail', async (req, res) => {
  const name = req.query.name
  const url = name ? `https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${name}` : 'https://www.thecocktaildb.com/api/json/v1/1/random.php'
  fetchUrl(url)
    .then(data => {
      const formattedData = data.drinks ? createResponse(data) : { status: 500 }
      res.send(formattedData)
    }).catch(e => {
      res.status(500)
    })
})


const fetchUrl = (url) => new Promise((resolve, reject) => {
  fetch(url)
    .then(response => response.json())
    .then(data => resolve(data))
    .catch(error => reject(error))
})

const createResponse = (data) => {
  const drink = data.drinks[0]
  const ingredients = formatIngredientsWithMeasurements(drink)
  return {
    name: drink.strDrink,
    thumb: drink.strDrinkThumb,
    instructions: drink.strInstructions,
    ingredients
  }
}

const formatIngredientsWithMeasurements = (drink) => {
  let ingredients = []
  for (let i = 1; i < 16; i++) {
    let ingredient = drink[`strIngredient${i}`]
    let measure = drink[`strMeasure${i}`]
    if (ingredient) {
      ingredients.push({ingredient, measure})
    }
  }
  return ingredients
}

app.listen(port, () => {
  console.log(`Cocktail app listening at http://localhost:${port}`)
})